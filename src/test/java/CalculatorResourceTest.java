import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+400";
        assertEquals(800, calculatorResource.sum(expression));

        expression = "300+99+45+44";
        assertEquals(488, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100-25";
        assertEquals(874, calculatorResource.subtraction(expression));

        expression = "20-2-5-7";
        assertEquals(6, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "20*46*12";
        assertEquals(11040, calculatorResource.multiplication(expression));

        expression = "14*17*7*13";
        assertEquals(21658, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "63/7/3/3";
        assertEquals(1, calculatorResource.division(expression));

        expression = "121/11/11";
        assertEquals(1, calculatorResource.division(expression));
    }
}
